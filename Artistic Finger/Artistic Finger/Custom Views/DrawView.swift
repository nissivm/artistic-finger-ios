//
//  DrawView.swift
//  Artistic Finger
//
//  Created by Nissi Vieira Miranda on 6/9/17.
//  Copyright © 2017 App Magic. All rights reserved.
//

import UIKit

class DrawView: UIView
{
    private var path: UIBezierPath!
    
    var drawingVC: DrawingViewController?
    
    var brushstrokes = [UIBezierPath]()
    var brushstrokeColorCodes = [String]()
    var brushstrokeThicknesses = [CGFloat]()
    
    var delBrushstrokes = [UIBezierPath]()
    var delBrushstrokeColorCodes = [String]()
    var delBrushstrokeThicknesses = [CGFloat]()
    
    var brushstrokeColorCode = "000000"
    var brushstrokeThickness: CGFloat = 20
    
    init(_ drawViewFrame: CGRect)
    {
        super.init(frame: drawViewFrame)
        
        let rect = CGRect(x: 0, y: 0, width: drawViewFrame.width, height: drawViewFrame.height)
        
        // Segura a imagem com todos os traços anteriormente desenhados:
        let drawingHolder = UIImageView(frame: rect)
        self.addSubview(drawingHolder)
        
        // Segura a imagem com o traço currently sendo desenhado:
        let drawingHolder2 = UIImageView(frame: rect)
        self.addSubview(drawingHolder2)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Drawing
    //-------------------------------------------------------------------------//
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if let touch = touches.first
        {
            clearRedo()
            
            if brushstrokeColorCodes.count == 0 // Undo até o fim e depois começou a desenhar
            {
                brushstrokeColorCodes.append(brushstrokeColorCode)
            }
            
            if brushstrokeThicknesses.count == 0 // Idem
            {
                brushstrokeThicknesses.append(brushstrokeThickness)
            }
            
            let touchPoint = touch.location(in: self)
            
            path = UIBezierPath()
            path.lineCapStyle = .round
            path.lineWidth = brushstrokeThickness
            path.move(to: touchPoint)
            updateCurrentDrawing()
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if let touch = touches.first
        {
            let touchPoint = touch.location(in: self)
            path.addLine(to: touchPoint)
            updateCurrentDrawing()
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if let touch = touches.first
        {
            let touchPoint = touch.location(in: self)
            path.addLine(to: touchPoint)
            updateCurrentDrawing()
            
            brushstrokes.append(path)
            
            if brushstrokes.count > brushstrokeColorCodes.count
            {
                brushstrokeColorCodes.append(brushstrokeColorCode)
                brushstrokeThicknesses.append(brushstrokeThickness)
            }
            
            updateDrawing()
            
            drawingVC?.undoButton.isEnabled = true
            drawingVC?.saveArtworkButton.isEnabled = true
            drawingVC?.clearArtworkButton.isEnabled = true
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Auxiliar
    //-------------------------------------------------------------------------//
    
    // Garante que o traço apareça na tela à medida que o usuário desenha:
    
    private func updateCurrentDrawing()
    {
        // Note: Using the one with options, the app crashes with a longer drawing:
        UIGraphicsBeginImageContext(self.frame.size)
        
        UIColor(hex: brushstrokeColorCode).setStroke()
        path.stroke(with: .normal, alpha: 1.0)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if let image = img
        {
            let drawingHolder2 = subviews[1] as! UIImageView
                drawingHolder2.image = image
        }
    }
    
    // Garante que os traços anteriores ficarão visíveis enquanto o usuário desenha novos:
    
    private func updateDrawing()
    {
        UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0.0)
        
        for (index, path) in brushstrokes.enumerated()
        {
            let code = brushstrokeColorCodes[index]
            let espessura = brushstrokeThicknesses[index]
            
            UIColor(hex: code).setStroke()
            path.lineWidth = espessura
            path.stroke(with: .normal, alpha: 1.0)
        }
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let drawingHolder2 = subviews[1] as! UIImageView
            drawingHolder2.image = nil
        
        if let image = img
        {
            let drawingHolder = subviews.first as! UIImageView
                drawingHolder.image = image
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Undo & Redo
    //-------------------------------------------------------------------------//
    
    func performUndo()
    {
        // Pessoa escolheu nova cor e/ou nova espessura e, ao invés de desenhar 
        // mais, deu undo novamente:
        
        if (brushstrokeColorCodes.count - brushstrokes.count) == 1
        {
            _ = brushstrokeColorCodes.popLast()
            _ = brushstrokeThicknesses.popLast()
        }
        
        delBrushstrokes.append(brushstrokes.last!)
        delBrushstrokeColorCodes.append(brushstrokeColorCodes.last!)
        delBrushstrokeThicknesses.append(brushstrokeThicknesses.last!)
        
        _ = brushstrokes.popLast()
        _ = brushstrokeColorCodes.popLast()
        _ = brushstrokeThicknesses.popLast()
        
        drawingVC?.redoButton.isEnabled = true
        
        if brushstrokes.count == 0
        {
            drawingVC?.undoButton.isEnabled = false
            brushstrokeColorCode = "000000"
            brushstrokeThickness = 20
            
            if drawingVC?.numOfStickersOnStage == 0
            {
                drawingVC?.saveArtworkButton.isEnabled = false
                drawingVC?.clearArtworkButton.isEnabled = false
            }
        }
        else
        {
            brushstrokeColorCode = brushstrokeColorCodes.last!
            brushstrokeThickness = brushstrokeThicknesses.last!
        }
        
        updateDrawing()
        
        let dic: [String : Any] = ["ColorCode" : brushstrokeColorCode,
                                   "Thikness" : brushstrokeThickness]
        
        let name = NSNotification.Name(rawValue: "PerformedUndoOrRedo")
        NotificationCenter.default.post(name: name, object: dic)
    }
    
    func performRedo()
    {
        brushstrokes.append(delBrushstrokes.last!)
        brushstrokeColorCodes.append(delBrushstrokeColorCodes.last!)
        brushstrokeThicknesses.append(delBrushstrokeThicknesses.last!)
        
        _ = delBrushstrokes.popLast()
        _ = delBrushstrokeColorCodes.popLast()
        _ = delBrushstrokeThicknesses.popLast()
        
        brushstrokeColorCode = brushstrokeColorCodes.last!
        brushstrokeThickness = brushstrokeThicknesses.last!
        
        drawingVC?.undoButton.isEnabled = true
        drawingVC?.saveArtworkButton.isEnabled = true
        drawingVC?.clearArtworkButton.isEnabled = true
        
        if delBrushstrokes.count == 0
        {
            drawingVC?.redoButton.isEnabled = false
        }
        
        updateDrawing()
        
        let dic: [String : Any] = ["ColorCode" : brushstrokeColorCode,
                                   "Thikness" : brushstrokeThickness]
        
        let name = NSNotification.Name(rawValue: "PerformedUndoOrRedo")
        NotificationCenter.default.post(name: name, object: dic)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Clear Undo & Redo
    //-------------------------------------------------------------------------//
    
    func clearUndo()
    {
        if brushstrokes.count > 0
        {
            brushstrokes.removeAll()
        }
        
        if brushstrokeColorCodes.count > 0
        {
            brushstrokeColorCodes.removeAll()
            brushstrokeThicknesses.removeAll()
        }
        
        brushstrokeColorCode = "000000"
        brushstrokeThickness = 20
        
        brushstrokeColorCodes.append(brushstrokeColorCode)
        brushstrokeThicknesses.append(brushstrokeThickness)
        
        drawingVC?.undoButton.isEnabled = false
        
        updateDrawing()
    }
    
    func clearRedo()
    {
        if delBrushstrokes.count > 0
        {
            delBrushstrokes.removeAll()
            delBrushstrokeColorCodes.removeAll()
            delBrushstrokeThicknesses.removeAll()
            
            drawingVC?.redoButton.isEnabled = false
        }
    }
}
