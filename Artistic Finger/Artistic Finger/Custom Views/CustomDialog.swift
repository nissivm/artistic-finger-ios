//
//  CustomDialog.swift
//  Artistic Finger
//
//  Created by Nissi Vieira Miranda on 6/8/17.
//  Copyright © 2017 App Magic. All rights reserved.
//

import UIKit

enum DialogType: String
{
    case AlbumPermission
    case PromptArtworkSaving
    case ArtworkSaved
    case ArtworkNotSaved
    case StartOver
    case PickAnotherPaper
    case None
}

protocol CustomDialogDelegate: class
{
    func giveAlbumPermission()
    func saveArtwork()
    func startOver()
    func pickAnotherPaper()
}

class CustomDialog: UIView
{
    weak var delegate: CustomDialogDelegate?
    private var background: UIView?
    
    class func instanceFromNib(_ dialogType: DialogType) -> CustomDialog?
    {
        var nib: UINib?
        
        switch dialogType
        {
            case .AlbumPermission:
                nib = UINib(nibName: "AlbumPermissionDialog", bundle: nil)
            
            case .PromptArtworkSaving:
                nib = UINib(nibName: "PromptArtworkSavingDialog", bundle: nil)
            
            case .ArtworkSaved:
                nib = UINib(nibName: "ArtworkSavedDialog", bundle: nil)
            
            case .ArtworkNotSaved:
                nib = UINib(nibName: "ArtworkNotSavedDialog", bundle: nil)
            
            case .StartOver:
                nib = UINib(nibName: "StartOverDialog", bundle: nil)
            
            case .PickAnotherPaper:
                nib = UINib(nibName: "PickAnotherPaperDialog", bundle: nil)
        
            default:
                print("\n Unknown Nib File \n")
        }
        
        if nib != nil
        {
            return nib!.instantiate(withOwner: nil, options: nil)[0] as? CustomDialog
        }
        else
        {
            return nil
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Auxiliar functions
    //-------------------------------------------------------------------------//
    
    func getCustomDialogBackground() -> UIView
    {
        let w = UIScreen.main.bounds.width
        let h = UIScreen.main.bounds.height
        let rect = CGRect(x: 0, y: 0, width: w, height: h)
        
        background = UIView(frame: rect)
        background!.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.7)
        background!.isUserInteractionEnabled = true
        
        return background!
    }
    
    func setCustomDialogFrame()
    {
        let w = UIScreen.main.bounds.width
        let h = UIScreen.main.bounds.height
        let selfW = self.bounds.width
        let selfH = self.bounds.height
        let x = (w - selfW)/2
        let y = (h - selfH)/2
        
        self.frame = CGRect(x: x, y: y, width: selfW, height: selfH)
    }
    
    private func removeAlert()
    {
        delegate = nil
        background!.removeFromSuperview()
        background = nil
        self.removeFromSuperview()
    }
    
    //-------------------------------------------------------------------------//
    // MARK: IBActions
    //-------------------------------------------------------------------------//
    
    @IBAction func givePermissionButtonTapped(_ sender: UIButton)
    {
        delegate?.giveAlbumPermission()
        removeAlert()
    }
    
    @IBAction func saveArtworkButtonTapped(_ sender: UIButton)
    {
        delegate?.saveArtwork()
        removeAlert()
    }
    
    @IBAction func startOverButtonTapped(_ sender: UIButton)
    {
        delegate?.startOver()
        removeAlert()
    }
    
    @IBAction func pickButtonTapped(_ sender: UIButton)
    {
        delegate?.pickAnotherPaper()
        removeAlert()
    }
    
    @IBAction func removeAlertButtonTapped(_ sender: UIButton)
    {
        removeAlert()
    }
}
