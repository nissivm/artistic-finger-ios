//
//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  Sticker.swift
//  Artistic Finger
//
//  Created by Nissi Vieira Miranda on 07/12/14.
//  Copyright (c) 2014 App Magic. All rights reserved.
//

import UIKit

class Sticker: UIImageView, UIGestureRecognizerDelegate
{
    private let notifCenter = NotificationCenter.default
    private var stageFrame: CGRect!
    private var currentCenterOnStage = CGPoint.zero
    private var insideStageBounds = true
    
    init?(_ stickerName: String, _ stageFrame: CGRect, _ panRecogn: UIPanGestureRecognizer)
    {
        guard let filepath = Bundle.main.path(forResource: stickerName, ofType: "png") else
        {
            return nil
        }
        
        guard let stickerImage = UIImage(contentsOfFile: filepath) else
        {
            return nil
        }
        
        super.init(image: stickerImage)
        
        isUserInteractionEnabled = true
        isMultipleTouchEnabled = true
        contentMode = .scaleAspectFit
        backgroundColor = UIColor.clear
        
        var sel = #selector(self.handleRotation)
        
        let rotationRecogn = UIRotationGestureRecognizer(target: self, action: sel)
            rotationRecogn.delegate = self
        
        sel = #selector(self.handlePinch)
        
        let pinchRecogn = UIPinchGestureRecognizer(target: self, action: sel)
            pinchRecogn.delegate = self
        
        sel = #selector(self.handleLongPress)
        
        let longPressRecogn = UILongPressGestureRecognizer(target: self, action: sel)
            longPressRecogn.delegate = self
            longPressRecogn.minimumPressDuration = 1.0
        
        gestureRecognizers = [rotationRecogn, pinchRecogn, longPressRecogn, panRecogn]
        
        self.stageFrame = stageFrame
        let stickerSize = self.stickerSize(for: image!)
        
        frame = CGRect(x: self.stageFrame.origin.x + 20.0,
                       y: self.stageFrame.origin.y + 20.0,
                       width: stickerSize.width,
                       height: stickerSize.height)
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Did move to superview
    //-------------------------------------------------------------------------//
    
    override func didMoveToSuperview()
    {
        if superview != nil
        {
            //print("\n Added to Superview \n")
            
            checkPosition()
        }
        else
        {
            //print("\n Removed from Superview \n")
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Checks if user is touching a sticker's opaque point or not
    //-------------------------------------------------------------------------//
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool
    {
        super.point(inside: point, with: event)
        
        if self.getAlphaValueAtPoint(point: point) > 0
        {
            return true
        }
        else
        {
            return false // Touches began is not called
        }
    }

    //-------------------------------------------------------------------------//
    // MARK: Touches Began
    //-------------------------------------------------------------------------//
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        let name = NSNotification.Name(rawValue: "MoveStickerToTheTop")
        notifCenter.post(name: name, object: self)
    }

    //-------------------------------------------------------------------------//
    // MARK: UIGestureRecognizerDelegate
    //-------------------------------------------------------------------------//
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
         shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool
    {
        return true
    }

    //-------------------------------------------------------------------------//
    // MARK: Rotation Gesture
    //-------------------------------------------------------------------------//
    
    func handleRotation(_ gestureRecognizer: UIRotationGestureRecognizer)
    {
        gestureRecognizer.view!.transform = gestureRecognizer.view!.transform.rotated(by: gestureRecognizer.rotation)
        gestureRecognizer.rotation = 0.0
    }

    //-------------------------------------------------------------------------//
    // MARK: Pinch Gesture
    //-------------------------------------------------------------------------//
    
    private var lastScale: CGFloat = 0.0
    
    func handlePinch(_ gestureRecognizer: UIPinchGestureRecognizer)
    {
        if gestureRecognizer.state == .began
        {
            // Reset the last scale, necessary if there are multiple objects with 
            // different scales:
            lastScale = gestureRecognizer.scale
        }
        
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed
        {
            let gestView = gestureRecognizer.view!
            let currentScale = gestView.layer.value(forKeyPath: "transform.scale") as! CGFloat
            
            let kMaxScale: CGFloat = 2.5 // Max zoom
            let kMinScale: CGFloat = 1.0 // Min zoom
            
            var newScale = 1 - (lastScale - gestureRecognizer.scale)
                newScale = min(newScale, kMaxScale / currentScale)
                newScale = max(newScale, kMinScale / currentScale)
            
            let transform = gestView.transform.scaledBy(x: newScale, y: newScale)
            gestureRecognizer.view!.transform = transform
            
            // Stores the previous scale factor for the next pinch gesture call:
            lastScale = gestureRecognizer.scale
            
            checkPosition()
        }
        
        if gestureRecognizer.state == .ended
        {
            if !insideStageBounds
            {
                let name = NSNotification.Name(rawValue: "RemoveStickerFromStage")
                notifCenter.post(name: name, object: nil)
            }
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Long Press Gesture
    //-------------------------------------------------------------------------//
    
    private var canFlip = true
    
    func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer)
    {
        guard canFlip else
        {
            return
        }
        
        canFlip = false
        
        UIGraphicsBeginImageContextWithOptions(image!.size, false, 0)
        
        guard let bitmap = UIGraphicsGetCurrentContext() else
        {
            setTimer()
            return
        }
        
        bitmap.scaleBy(x: -1, y: -1)
        
        let rect = CGRect(x: image!.size.width * -1,
                          y: image!.size.height * -1,
                          width: image!.size.width,
                          height: image!.size.height)
        
        bitmap.draw(image!.cgImage!, in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if newImage != nil
        {
            image = newImage
        }
        
        setTimer()
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Timer
    //-------------------------------------------------------------------------//
    
    private func setTimer()
    {
        let sel = #selector(self.fireTimer)
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: sel, userInfo: nil, repeats: false)
    }
    
    func fireTimer()
    {
        canFlip = true
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Getters
    //-------------------------------------------------------------------------//
    
    func getCurrentCenterOnStage() -> CGPoint
    {
        return currentCenterOnStage
    }
    
    func isInsideStageBounds() -> Bool
    {
        return insideStageBounds
    }

    //-------------------------------------------------------------------------//
    // MARK: Auxiliar Methods
    //-------------------------------------------------------------------------//
    
    func checkPosition()
    {
        insideStageBounds = isInStageBounds()
        
        if insideStageBounds
        {
            alpha = 1.0
            
            let stage = UIView(frame: stageFrame)
            currentCenterOnStage = superview!.convert(center, to: stage)
        }
        else
        {
            alpha = 0.5
        }
    }

    private func isInStageBounds() -> Bool
    {
        let stickerY = frame.origin.y
        let stickerX = frame.origin.x
        let stickerW = frame.size.width
        let stickerH = frame.size.height
        
        let stageY = stageFrame.origin.y
        let stageX = stageFrame.origin.x
        let stageW = stageFrame.size.width
        let stageH = stageFrame.size.height
        
        var tolerance: CGFloat = 50.0
        
        if UIScreen.main.scale > 1
        {
            tolerance = 100.0
        }
        
        let insideTopLimit = stickerY >= (stageY - tolerance)
        let insideLeftLimit = stickerX >= (stageX - tolerance)
        let insideBottomLimit = (stickerY + stickerH) <= (stageY + stageH + tolerance)
        let insideRightLimit = (stickerX + stickerW) <= (stageX + stageW + tolerance)
        
        if insideTopLimit && insideLeftLimit && insideBottomLimit && insideRightLimit
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    private func stickerSize(for imagem: UIImage) -> CGSize
    {
        let minSize: CGFloat = 200.0
        var tamanho = CGSize.zero
        
        if imagem.size.width > imagem.size.height
        {
            let stickerHeight = (minSize * imagem.size.height)/imagem.size.width
            tamanho = CGSize(width: minSize, height: stickerHeight)
        }
        else if imagem.size.width < imagem.size.height
        {
            let stickerWidth = (minSize * imagem.size.width)/imagem.size.height
            tamanho = CGSize(width: stickerWidth, height: minSize)
        }
        else
        {
            tamanho = CGSize(width: CGFloat(minSize), height: CGFloat(minSize))
        }

        return tamanho
    }
}
