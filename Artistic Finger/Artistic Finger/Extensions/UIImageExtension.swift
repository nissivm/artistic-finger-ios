//
//  UIImageExtension.swift
//  Artistic Finger
//
//  Created by Nissi Vieira Miranda on 6/10/17.
//  Copyright © 2017 App Magic. All rights reserved.
//

import Foundation

extension UIImage
{
    func cropSelfTo(cropArea: CGRect) -> UIImage?
    {
        guard let cgImg = self.cgImage else
        {
            return nil
        }
        
        var rect = cropArea
            rect.origin.x *= self.scale
            rect.origin.y *= self.scale
            rect.size.width *= self.scale
            rect.size.height *= self.scale
        
        guard let imageRef: CGImage = cgImg.cropping(to: rect) else
        {
            return nil
        }
        
        return UIImage(cgImage: imageRef, scale: 0, orientation: self.imageOrientation)
    }
    
    func stickerToIcon() -> UIImage?
    {
        let newWidth = self.size.width/4
        let newHeight = (newWidth * self.size.height)/self.size.width
        let newSize = CGSize(width: newWidth, height: newHeight)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImg
    }
}
