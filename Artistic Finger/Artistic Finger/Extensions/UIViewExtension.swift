//
//  UIViewExtension.swift
//  Artistic Finger
//
//  Created by Nissi Vieira Miranda on 5/11/17.
//  Copyright © 2017 App Magic. All rights reserved.
//

import Foundation

extension UIView
{
    func getPixelColorAtPoint(point: CGPoint) -> UIColor
    {
        let pixel = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: 4)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        
        let context = CGContext(data: pixel, width: 1, height: 1,
                                bitsPerComponent: 8, bytesPerRow: 4,
                                space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        
            context!.translateBy(x: -point.x, y: -point.y)
        
        self.layer.render(in: context!)
        
        let red = CGFloat(pixel[0])/255.0
        let green = CGFloat(pixel[1])/255.0
        let blue = CGFloat(pixel[2])/255.0
        let alpha = CGFloat(pixel[3])/255.0
        
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        pixel.deallocate(capacity: 4)
        
        return color
    }
    
    func getAlphaValueAtPoint(point: CGPoint) -> CGFloat
    {
        let pixel = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: 4)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        
        let context = CGContext(data: pixel, width: 1, height: 1,
                                bitsPerComponent: 8, bytesPerRow: 4,
                                space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        
            context!.translateBy(x: -point.x, y: -point.y)
        
        self.layer.render(in: context!)
        let alpha = CGFloat(pixel[3])/255.0
        pixel.deallocate(capacity: 4)
        
        return alpha
    }
    
    func applyMask(maskImageName: String)
    {
        let maskImage = UIImage(named: maskImageName)
        
        let maskImageView = UIImageView(image: maskImage)
            maskImageView.contentMode = .scaleAspectFit
            maskImageView.frame = self.bounds
        
        self.layer.mask = maskImageView.layer
    }
    
    func getImageRepresentation() -> UIImage?
    {
        UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0.0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return img
    }
}
