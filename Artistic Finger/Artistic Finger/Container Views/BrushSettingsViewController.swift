//
//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  BrushSettingsViewController.swift
//  Artistic Finger
//
//  Created by Nissi Vieira Miranda on 30/11/14.
//  Copyright (c) 2014 App Magic. All rights reserved.
//

import UIKit

class BrushSettingsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var canvas: UIView!
    @IBOutlet weak var bigBrushPaint: UIView!
    @IBOutlet weak var mediumBrushPaint: UIView!
    @IBOutlet weak var smallBrushPaint: UIView!
    
    @IBOutlet weak var collectionViewWidthConstraint: NSLayoutConstraint!
    
    private let center = NotificationCenter.default
    private var currentBrush: UIButton!
    
    private var currentBrushstrokeColorCode = "000000"
    private var currentBrushstrokeThickness: CGFloat = 20
    
    private var newBrushstrokeColorCode = "000000"
    private var newBrushstrokeThickness: CGFloat = 20
    
    private var currentBrushstrokeColorIdx = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        var sel = #selector(self.performedUndoOrRedo(_:))
        var name = NSNotification.Name(rawValue: "PerformedUndoOrRedo")
        center.addObserver(self, selector: sel, name: name, object: nil)
        
        sel = #selector(self.stageColorChanged(_:))
        name = NSNotification.Name(rawValue: "StageColorChanged")
        center.addObserver(self, selector: sel, name: name, object: nil)
        
        bigBrushPaint.applyMask(maskImageName: "PaintOnBrushMask")
        mediumBrushPaint.applyMask(maskImageName: "PaintOnBrushMask")
        smallBrushPaint.applyMask(maskImageName: "PaintOnBrushMask")
        
        bigBrushPaint.backgroundColor = UIColor(hex: currentBrushstrokeColorCode)
        mediumBrushPaint.backgroundColor = UIColor(hex: currentBrushstrokeColorCode)
        smallBrushPaint.backgroundColor = UIColor(hex: currentBrushstrokeColorCode)
        
        togglePaintOnBrushes()
        
        currentBrush = view.viewWithTag(101) as! UIButton
        
        currentBrushstrokeColorIdx = Constants.hexColors.index(of: currentBrushstrokeColorCode)!
        
        scrollCollectionForIndex(currentBrushstrokeColorIdx)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        if UIScreen.main.bounds.width == 1366.0 // iPad Pro 12.9 inches
        {
            collectionViewWidthConstraint.constant = 680.0
        }
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    deinit
    {
        center.removeObserver(self)
    }

    //----------------------------------------------------------------------------------//
    // MARK: Notifications
    //----------------------------------------------------------------------------------//
    
    func performedUndoOrRedo(_ notification: Notification)
    {
        let dic = notification.object as! [String : Any]
        let colorCode = dic["ColorCode"] as! String
        let thickness = dic["Thikness"] as! CGFloat
        
        currentBrushstrokeColorCode = colorCode
        newBrushstrokeColorCode = colorCode
        
        currentBrushstrokeColorIdx = Constants.hexColors.index(of: currentBrushstrokeColorCode)!
        
        collectionView.reloadData()
        scrollCollectionForIndex(currentBrushstrokeColorIdx)
        
        currentBrushstrokeThickness = thickness
        newBrushstrokeThickness = thickness
        
        bigBrushPaint.backgroundColor = UIColor(hex: currentBrushstrokeColorCode)
        mediumBrushPaint.backgroundColor = UIColor(hex: currentBrushstrokeColorCode)
        smallBrushPaint.backgroundColor = UIColor(hex: currentBrushstrokeColorCode)
        
        togglePaintOnBrushes()
        
        currentBrush.isEnabled = true
        
        var brush: UIButton!
        
        if currentBrushstrokeThickness == 30
        {
            brush = view.viewWithTag(100) as! UIButton
            brush.isEnabled = false
        }
        else if currentBrushstrokeThickness == 20
        {
            brush = view.viewWithTag(101) as! UIButton
            brush.isEnabled = false
        }
        else if currentBrushstrokeThickness == 10
        {
            brush = view.viewWithTag(102) as! UIButton
            brush.isEnabled = false
        }
        
        currentBrush = brush
    }
    
    func stageColorChanged(_ notification: Notification)
    {
        let colorCode = notification.object as! String
        canvas.backgroundColor = UIColor(hex: colorCode)
    }

    //----------------------------------------------------------------------------------//
    // MARK: IBActions
    //----------------------------------------------------------------------------------//
    
    @IBAction func closeButtonTapped(_ sender: UIButton)
    {
        scrollCollectionForIndex(currentBrushstrokeColorIdx)
        
        if (currentBrushstrokeColorCode != newBrushstrokeColorCode) ||
           (currentBrushstrokeThickness != newBrushstrokeThickness)
        {
            currentBrushstrokeColorCode = newBrushstrokeColorCode
            currentBrushstrokeThickness = newBrushstrokeThickness
            
            let name = NSNotification.Name(rawValue: "BrushSettingsChanged")
            
            let dic: [String : Any] = ["ColorCode" : currentBrushstrokeColorCode,
                                       "Thikness" : currentBrushstrokeThickness]
            
            center.post(name: name, object: dic)
        }
        else
        {
            let name = NSNotification.Name(rawValue: "CloseContainerView")
            center.post(name: name, object: nil)
        }
    }

    @IBAction func brushTapped(_ sender: UIButton)
    {
        guard sender.isEnabled else
        {
            return
        }
        
        currentBrush.isEnabled = true
        sender.isEnabled = false
        currentBrush = sender
        
        if sender.tag == 100
        {
            newBrushstrokeThickness = 30
        }
        else if sender.tag == 101
        {
            newBrushstrokeThickness = 20
        }
        else if sender.tag == 102
        {
            newBrushstrokeThickness = 10
        }
        
        togglePaintOnBrushes()
    }

    //----------------------------------------------------------------------------------//
    // MARK: Auxiliar Methods
    //----------------------------------------------------------------------------------//

    private func togglePaintOnBrushes()
    {
        if newBrushstrokeThickness == 30
        {
            bigBrushPaint.isHidden = false
            mediumBrushPaint.isHidden = true
            smallBrushPaint.isHidden = true
        }
        else if newBrushstrokeThickness == 20
        {
            bigBrushPaint.isHidden = true
            mediumBrushPaint.isHidden = false
            smallBrushPaint.isHidden = true
        }
        else if newBrushstrokeThickness == 10
        {
            bigBrushPaint.isHidden = true
            mediumBrushPaint.isHidden = true
            smallBrushPaint.isHidden = false
        }
    }
    
    private func scrollCollectionForIndex(_ idx: Int)
    {
        let idxPath = IndexPath(item: idx, section: 0)
        var position: UICollectionViewScrollPosition!
        
        if idx <= 5
        {
            position = UICollectionViewScrollPosition.top
        }
        else if idx >= (Constants.hexColors.count - 6)
        {
            position = UICollectionViewScrollPosition.bottom
        }
        else
        {
            position = UICollectionViewScrollPosition.centeredVertically
        }
        
        collectionView.scrollToItem(at: idxPath, at: position, animated: true)
    }

    //----------------------------------------------------------------------------------//
    // MARK: UICollectionViewDataSource
    //----------------------------------------------------------------------------------//

    func collectionView(_ view: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return Constants.hexColors.count
    }

    func collectionView(_ cv: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let hexColor = Constants.hexColors[indexPath.item]
        let color = UIColor(hex: hexColor)
        
        let cell = cv.dequeueReusableCell(withReuseIdentifier: "PaintCell", for: indexPath) as! PaintCell
            cell.circle.applyMask(maskImageName: "CircleMask")
            cell.paint.applyMask(maskImageName: "PaintSplashMask")
            cell.circle.backgroundColor = color
            cell.paint.backgroundColor = color
        
        if hexColor == newBrushstrokeColorCode
        {
            cell.circle.isHidden = true
            cell.paint.isHidden = false
        }
        else
        {
            cell.circle.isHidden = false
            cell.paint.isHidden = true
        }
        
        return cell
    }

    //-------------------------------------------------------------------------//
    // MARK: UICollectionViewDelegate
    //-------------------------------------------------------------------------//
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        currentBrushstrokeColorIdx = indexPath.item
        
        let hexColor = Constants.hexColors[indexPath.item]
        let color = UIColor(hex: hexColor)
        
        newBrushstrokeColorCode = hexColor
        bigBrushPaint.backgroundColor = color
        mediumBrushPaint.backgroundColor = color
        smallBrushPaint.backgroundColor = color
        
        collectionView.reloadData()
    }

    //-------------------------------------------------------------------------//
    // MARK: Memory Warning
    //-------------------------------------------------------------------------//
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
