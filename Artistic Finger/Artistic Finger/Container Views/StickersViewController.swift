//
//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  StickersViewController.swift
//  Artistic Finger
//
//  Created by Nissi Vieira Miranda on 03/12/14.
//  Copyright (c) 2014 App Magic. All rights reserved.
//

import UIKit

class StickersViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{
    @IBOutlet weak var blurredBackground: UIImageView!
    @IBOutlet weak var animationsButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let defaults = UserDefaults.standard
    private let center = NotificationCenter.default
    private var buttonsList = [UIButton]()
    
    private var cityNames = [String]()
    private var landAnimalsNames = [String]()
    private var mixNames = [String]()
    private var seaAnimalsNames = [String]()
    private var transportNames = [String]()
    
    private var cityIconImages = [UIImage?]()
    private var landAnimalsIconImages = [UIImage?]()
    private var mixIconImages = [UIImage?]()
    private var seaAnimalsIconImages = [UIImage?]()
    private var transportIconImages = [UIImage?]()
    
    private var selButtonTag = 400

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let sel = #selector(self.stickersVCExposed)
        let name = NSNotification.Name(rawValue: "StickersVCExposed")
        center.addObserver(self, selector: sel, name: name, object: nil)
        
        if defaults.string(forKey: "Animation Status") == nil
        {
            defaults.set("On", forKey: "Animation Status")
        }
        
        // Fill names arrays:
        
        var counter = 1
        var baseName = "city_"
        
        while counter < 28
        {
            let fullName = "\(baseName)\(counter)@2x"
            
            if counter == 1
            {
                let imgView = view.viewWithTag(410) as! UIImageView
                    imgView.image = getIconForImage(name: fullName)
            }
            
            cityNames.append(fullName)
            counter += 1
        }
        
        counter = 1
        baseName = "land_animals_"
        
        while counter < 17
        {
            let fullName = "\(baseName)\(counter)@2x"
            
            if counter == 1
            {
                let imgView = view.viewWithTag(411) as! UIImageView
                    imgView.image = getIconForImage(name: fullName)
            }
            
            landAnimalsNames.append(fullName)
            counter += 1
        }
        
        counter = 1
        baseName = "mix_"
        
        while counter < 25
        {
            let fullName = "\(baseName)\(counter)@2x"
            
            if counter == 1
            {
                let imgView = view.viewWithTag(412) as! UIImageView
                    imgView.image = getIconForImage(name: fullName)
            }
            
            mixNames.append(fullName)
            counter += 1
        }
        
        counter = 1
        baseName = "sea_animals_"
        
        while counter < 14
        {
            let fullName = "\(baseName)\(counter)@2x"
            
            if counter == 1
            {
                let imgView = view.viewWithTag(413) as! UIImageView
                    imgView.image = getIconForImage(name: fullName)
            }
            
            seaAnimalsNames.append(fullName)
            counter += 1
        }
        
        counter = 1
        baseName = "transport_"
        
        while counter < 17
        {
            let fullName = "\(baseName)\(counter)@2x"
            
            if counter == 1
            {
                let imgView = view.viewWithTag(414) as! UIImageView
                    imgView.image = getIconForImage(name: fullName)
            }
            
            transportNames.append(fullName)
            counter += 1
        }
        
        // Animations button:
        
        let animationStatus = defaults.string(forKey: "Animation Status")!
        
        if animationStatus == "Off"
        {
            let img = UIImage(named: "AnimationsOff")
            animationsButton.setImage(img, for: UIControlState())
        }
        
        // Stickers buttons:
        
        counter = 400
        
        while counter < 405
        {
            let btn = view.viewWithTag(counter) as! UIButton
            buttonsList.append(btn)
            counter += 1
        }
        
        // Fill city icon images array:
        
        for name in cityNames
        {
            let stickerIcon = getIconForImage(name: name)
            cityIconImages.append(stickerIcon)
        }
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    deinit
    {
        center.removeObserver(self)
    }

    //----------------------------------------------------------------------------------//
    // MARK: Notifications
    //----------------------------------------------------------------------------------//
    
    func stickersVCExposed()
    {
        var counter = 0
        
        while counter < buttonsList.count
        {
            let btn = buttonsList[counter]
            
            if counter == 0
            {
                btn.isEnabled = false
            }
            else
            {
                btn.isEnabled = true
            }
            
            counter += 1
        }
        
        selButtonTag = 400
        collectionView.reloadData()
        startScrollAnimation()
    }

    //----------------------------------------------------------------------------------//
    // MARK: IBActions
    //----------------------------------------------------------------------------------//
    
    @IBAction func closeButtonTapped(_ sender: UIButton)
    {
        let name = NSNotification.Name(rawValue: "CloseContainerView")
        center.post(name: name, object: nil)
        
        selButtonTag = 405
        emptyIconsArrays()
        collectionView.reloadData()
    }

    @IBAction func stickerButtonTapped(_ sender: UIButton)
    {
        guard sender.isEnabled else
        {
            return
        }
        
        var counter = 0
        
        while counter < buttonsList.count
        {
            let btn = buttonsList[counter]
            
            if btn.tag == sender.tag
            {
                btn.isEnabled = false
            }
            else
            {
                btn.isEnabled = true
            }
            
            counter += 1
        }
        
        selButtonTag = sender.tag
        
        if selButtonTag == 401
        {
            if landAnimalsIconImages.count == 0
            {
                for name in landAnimalsNames
                {
                    let stickerIcon = getIconForImage(name: name)
                    landAnimalsIconImages.append(stickerIcon)
                }
            }
        }
        else if selButtonTag == 402
        {
            if mixIconImages.count == 0
            {
                for name in mixNames
                {
                    let stickerIcon = getIconForImage(name: name)
                    mixIconImages.append(stickerIcon)
                }
            }
        }
        else if selButtonTag == 403
        {
            if seaAnimalsIconImages.count == 0
            {
                for name in seaAnimalsNames
                {
                    let stickerIcon = getIconForImage(name: name)
                    seaAnimalsIconImages.append(stickerIcon)
                }
            }
        }
        else if selButtonTag == 404
        {
            if transportIconImages.count == 0
            {
                for name in transportNames
                {
                    let stickerIcon = getIconForImage(name: name)
                    transportIconImages.append(stickerIcon)
                }
            }
        }
        
        collectionView!.reloadData()
        startScrollAnimation()
    }
    
    @IBAction func animationsButtonTapped(_ sender: UIButton)
    {
        let animationStatus = defaults.string(forKey: "Animation Status")!
        
        if animationStatus == "On"
        {
            defaults.set("Off", forKey: "Animation Status")
            
            let img = UIImage(named: "AnimationsOff")
            animationsButton.setImage(img, for: UIControlState())
        }
        else
        {
            defaults.set("On", forKey: "Animation Status")
            
            let img = UIImage(named: "AnimationsOn")
            animationsButton.setImage(img, for: UIControlState())
        }
    }
    
    //----------------------------------------------------------------------------------//
    // MARK: Gets
    //----------------------------------------------------------------------------------//
    
    private func getCurrentNamesArr() -> [String]
    {
        if selButtonTag == 400
        {
            return cityNames
        }
        else if selButtonTag == 401
        {
            return landAnimalsNames
        }
        else if selButtonTag == 402
        {
            return mixNames
        }
        else if selButtonTag == 403
        {
            return seaAnimalsNames
        }
        else if selButtonTag == 404
        {
            return transportNames
        }
        else
        {
            return [String]()
        }
    }
    
    private func getCurrentIconImagesArr() -> [UIImage?]
    {
        if selButtonTag == 400
        {
            return cityIconImages
        }
        else if selButtonTag == 401
        {
            return landAnimalsIconImages
        }
        else if selButtonTag == 402
        {
            return mixIconImages
        }
        else if selButtonTag == 403
        {
            return seaAnimalsIconImages
        }
        else if selButtonTag == 404
        {
            return transportIconImages
        }
        else
        {
            return [UIImage?]()
        }
    }
    
    private func getIconForImage(name: String) -> UIImage?
    {
        var iconImg: UIImage?
        
        if let filepath = Bundle.main.path(forResource: name, ofType: "png")
        {
            if let stickerImage = UIImage(contentsOfFile: filepath)
            {
                iconImg = stickerImage.stickerToIcon()
            }
        }
        
        return iconImg
    }
    
    //----------------------------------------------------------------------------------//
    // MARK: Animations
    //----------------------------------------------------------------------------------//
    
    private var blockingView: UIView?
    
    private func startScrollAnimation()
    {
        let animationStatus = defaults.string(forKey: "Animation Status")!
        
        guard animationStatus == "On" else
        {
            return
        }
        
        blockingView = UIView(frame: view.frame)
        blockingView!.backgroundColor = UIColor.clear
        blockingView!.isUserInteractionEnabled = true
        view.addSubview(blockingView!)
        
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self,
                                 selector: #selector(runTimer),
                                 userInfo: nil, repeats: false)
    }
    
    func runTimer()
    {
        let idxPath = IndexPath(item: getCurrentNamesArr().count - 1, section: 0)
        collectionView.scrollToItem(at: idxPath, at: .bottom, animated: true)
        
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self,
                                 selector: #selector(runTimer2),
                                 userInfo: nil, repeats: false)
    }
    
    func runTimer2()
    {
        let idxPath = IndexPath(item: 0, section: 0)
        collectionView.scrollToItem(at: idxPath, at: .top, animated: true)
        
        blockingView!.removeFromSuperview()
        blockingView = nil
    }

    //----------------------------------------------------------------------------------//
    // MARK: UICollectionViewDataSource
    //----------------------------------------------------------------------------------//

    func collectionView(_ cv: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return getCurrentIconImagesArr().count
    }

    func collectionView(_ cv: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = cv.dequeueReusableCell(withReuseIdentifier: "StickerCell", for: indexPath) as! StickerCell
            cell.stickerImageView.image = getCurrentIconImagesArr()[indexPath.item]
        
        return cell
    }

    //-------------------------------------------------------------------------//
    // MARK: UICollectionViewDelegate
    //-------------------------------------------------------------------------//
    
    func collectionView(_ cv: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let stickerName = getCurrentNamesArr()[indexPath.item]
        let name = NSNotification.Name(rawValue: "PlaceStickerOnStage")
        center.post(name: name, object: stickerName)
        
        selButtonTag = 405
        emptyIconsArrays()
        collectionView.reloadData()
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Empty Icons Arrays
    //-------------------------------------------------------------------------//
    
    private func emptyIconsArrays()
    {
        if landAnimalsIconImages.count > 0
        {
            landAnimalsIconImages.removeAll()
        }
        
        if mixIconImages.count > 0
        {
            mixIconImages.removeAll()
        }
        
        if seaAnimalsIconImages.count > 0
        {
            seaAnimalsIconImages.removeAll()
        }
        
        if transportIconImages.count > 0
        {
            transportIconImages.removeAll()
        }
    }

    //-------------------------------------------------------------------------//
    // MARK: Memory Warning
    //-------------------------------------------------------------------------//
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
