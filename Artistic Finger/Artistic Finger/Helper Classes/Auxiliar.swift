//
//  Auxiliar.swift
//  Artistic Finger
//
//  Created by Nissi Vieira Miranda on 5/8/17.
//  Copyright © 2017 App Magic. All rights reserved.
//

import Foundation
import Photos

class Auxiliar
{
    //-------------------------------------------------------------------------//
    // MARK: Get Picked Stage
    //-------------------------------------------------------------------------//
    
    static func getPickedStage(_ type: StageType, _ colorCode: String) -> DrawView
    {
        var frame: CGRect!
        let toolbarHeight: CGFloat = 100.0
        let screenWidth = UIScreen.main.bounds.width
        let usefulHeight = UIScreen.main.bounds.height - toolbarHeight
        
        switch type
        {
            case .Landscape:
                frame = CGRect(x: 0, y: 0, width: screenWidth, height: usefulHeight)
            
            case .Squared:
                let x = (screenWidth - usefulHeight)/2
                frame = CGRect(x: x, y: 0, width: usefulHeight, height: usefulHeight)
            
            case .Panoramic:
                let h = usefulHeight/2
                frame = CGRect(x: 0, y: h/2, width: screenWidth, height: h)
        
            default:
                frame = CGRect(x: 0, y: 0, width: 0, height: 0)
                print("\n Unknown Stage Type \n")
        }
        
        let drawView = DrawView(frame)
            drawView.isUserInteractionEnabled = true
            drawView.backgroundColor = UIColor(hex: colorCode)
        
        return drawView
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Album access error dialog
    //-------------------------------------------------------------------------//
    
    static func albumAccessErrorDialog(status: PHAuthorizationStatus,
                                       vc: UIViewController)
    {
        if status == .denied
        {
            let title = "Permission to access photo album is currently not allowed"
            let msg = "To allow this access and enable artwork saving:\n" +
                      "1. Go to your Settings.\n" +
                      "2. Scroll down, find Artistic Finger and tap on it.\n" +
                      "3. On the right panel, turn Photos On."
            
            let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
            
            let action = UIAlertAction(title: "Take me there!", style: .default)
            {
                (action) in
                
                if let appSettings = URL(string: UIApplicationOpenSettingsURLString)
                {
                    UIApplication.shared.open(appSettings, options: [:], completionHandler: nil)
                }
            }
            
            let action2 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(action)
            alert.addAction(action2)
            
            vc.present(alert, animated: true, completion: nil)
        }
        else
        {
            let title = "Cannot access photo album"
            let msg = "Is not possible to access photos album"
            
            presentAlertController(title, message: msg, vc: vc)
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: MBProgressHUD
    //-------------------------------------------------------------------------//
    
    static private var progressHud: MBProgressHUD?
    
    static func showLoadingHUDWithText(_ text: String, view: UIView)
    {
        DispatchQueue.main.async
        {
            if progressHud == nil
            {
                progressHud = MBProgressHUD.showAdded(to: view, animated: true)
            }
            
            progressHud!.labelText = text
        }
    }
    
    static func hideLoadingHUDInView(view: UIView)
    {
        DispatchQueue.main.async
        {
            if progressHud != nil
            {
                MBProgressHUD.hideAllHUDs(for: view, animated: true)
                progressHud = nil
            }
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: "Ok" Alert Controller
    //-------------------------------------------------------------------------//
    
    static func presentAlertController(_ title: String, message: String,
                                       vc: UIViewController)
    {
        let alert = UIAlertController(title: title, message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        let alertAction = UIAlertAction(title: "Ok", style: .default)
        {
            (alert) -> Void in
        }
        
        alert.addAction(alertAction)
        
        vc.present(alert, animated: true, completion: nil)
    }
}
