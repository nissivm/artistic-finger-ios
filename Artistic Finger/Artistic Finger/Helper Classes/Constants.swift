//
//  Constants.swift
//  Artistic Finger
//
//  Created by Nissi Vieira Miranda on 5/6/17.
//  Copyright © 2017 App Magic. All rights reserved.
//

import Foundation

enum StageType: String
{
    case Landscape
    case Squared
    case Panoramic
    case None
}

class Constants
{
    static let hexColors = ["fff100", "ecd85e", "d2af47", "ef7517", "eaa72e", "f2c301",
                            "c61d23", "d94129", "ad2d28", "cc0784", "dc3565", "e791b7",
                            "733864", "795586", "807bb4", "2f236c", "45337e", "585397",
                            "006cb2", "00a5e3", "bae0ff", "355c34", "398d44", "88c544",
                            "009589", "51b093", "91cbb2", "603813", "8a5d3b", "ba812c",
                            "96a0a5", "ffffff", "000000"]
}
