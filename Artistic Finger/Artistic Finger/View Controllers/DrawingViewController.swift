//
//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  DrawingViewController.swift
//  Artistic Finger
//
//  Created by Nissi Vieira Miranda on 30/11/14.
//  Copyright (c) 2014 App Magic. All rights reserved.
//

import UIKit
import Photos

enum ContViewName: String
{
    case BrushSettings
    case Stickers
    case None
}

class DrawingViewController: UIViewController, UIGestureRecognizerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, CustomDialogDelegate
{
    @IBOutlet weak var artworkArea: UIView!
    
    @IBOutlet weak var saveArtworkButton: UIButton!
    @IBOutlet weak var clearArtworkButton: UIButton!
    @IBOutlet weak var undoButton: UIButton!
    @IBOutlet weak var redoButton: UIButton!
    @IBOutlet weak var toolbar: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var stageColorsView: UIView!
    
    @IBOutlet weak var brushSettingsContViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var brushSettingsContViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var stickersContViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var stickersContViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var toolbarBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var stageColorsViewBottomConstraint: NSLayoutConstraint!
    
    private let center = NotificationCenter.default
    private var drawView: DrawView!
    private var showingContView: ContViewName = .None
    
    var numOfStickersOnStage = 0
    private var currentStickerOnTop: Sticker?
    private var currentStickerOnTopCenter: CGPoint!
    
    var stageType: StageType!
    var stageColorCode: String!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        
        var sel = #selector(self.closeContainerView)
        var name = NSNotification.Name(rawValue: "CloseContainerView")
        center.addObserver(self, selector: sel, name: name, object: nil)
        
        sel = #selector(self.brushSettingsChanged(_:))
        name = NSNotification.Name(rawValue: "BrushSettingsChanged")
        center.addObserver(self, selector: sel, name: name, object: nil)
        
        sel = #selector(self.placeStickerOnStage(_:))
        name = NSNotification.Name(rawValue: "PlaceStickerOnStage")
        center.addObserver(self, selector: sel, name: name, object: nil)
        
        sel = #selector(self.moveStickerToTheTop(_:))
        name = NSNotification.Name(rawValue: "MoveStickerToTheTop")
        center.addObserver(self, selector: sel, name: name, object: nil)
        
        sel = #selector(self.removeStickerFromStage)
        name = NSNotification.Name(rawValue: "RemoveStickerFromStage")
        center.addObserver(self, selector: sel, name: name, object: nil)
        
        
        drawView = Auxiliar.getPickedStage(stageType, stageColorCode)
        drawView.brushstrokeColorCodes.append(drawView.brushstrokeColorCode)
        drawView.brushstrokeThicknesses.append(drawView.brushstrokeThickness)
        drawView.drawingVC = self
        artworkArea.addSubview(drawView)
        
        
        name = NSNotification.Name(rawValue: "StageColorChanged")
        center.post(name: name, object: stageColorCode)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        let h = UIScreen.main.bounds.height
        
        brushSettingsContViewHeightConstraint.constant = h
        brushSettingsContViewTopConstraint.constant = h * -1
        stickersContViewHeightConstraint.constant = h
        stickersContViewTopConstraint.constant = h * -1
        
        let idx: Int = Constants.hexColors.index(of: stageColorCode)!
        let idxPath = IndexPath(item: idx, section: 0)
        collectionView.scrollToItem(at: idxPath, at: .centeredHorizontally, animated: true)
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    deinit
    {
        print("\n deinit called (DrawingVC) \n")
        center.removeObserver(self)
    }

    //-------------------------------------------------------------------------//
    // MARK: Notifications
    //-------------------------------------------------------------------------//
    
    func closeContainerView()
    {
        performContainerViewAnimation(false)
        showingContView = .None
    }
    
    func brushSettingsChanged(_ notification: Notification)
    {
        closeContainerView()
        
        let dic = notification.object as! [String : Any]
        let colorCode = dic["ColorCode"] as! String
        let thickness = dic["Thikness"] as! CGFloat
        
        drawView.brushstrokeColorCode = colorCode
        drawView.brushstrokeThickness = thickness
        
        if drawView.brushstrokes.count == 0
        {
            drawView.brushstrokeColorCodes.removeAll()
            drawView.brushstrokeThicknesses.removeAll()
        }
        else
        {
            if (drawView.brushstrokeColorCodes.count - drawView.brushstrokes.count) == 1
            {
                _ = drawView.brushstrokeColorCodes.popLast()
            }
            
            if (drawView.brushstrokeThicknesses.count - drawView.brushstrokes.count) == 1
            {
                _ = drawView.brushstrokeThicknesses.popLast()
            }
        }
        
        drawView.brushstrokeColorCodes.append(colorCode)
        drawView.brushstrokeThicknesses.append(thickness)
        
        drawView.clearRedo()
    }
    
    func placeStickerOnStage(_ notification: Notification)
    {
        closeContainerView()
        
        let stickerName = notification.object as! String
        
        let sel = #selector(self.handlePan)
        let panRecogn = UIPanGestureRecognizer(target: self, action: sel)
            panRecogn.delegate = self
        
        guard let newS = Sticker(stickerName, drawView.frame, panRecogn) else
        {
            return
        }
        
        newS.tag = 500 + numOfStickersOnStage
        artworkArea.addSubview(newS)
        
        currentStickerOnTop = newS
        currentStickerOnTopCenter = newS.center
        
        numOfStickersOnStage += 1
        
        saveArtworkButton.isEnabled = true
        clearArtworkButton.isEnabled = true
    }
    
    func moveStickerToTheTop(_ notification: Notification)
    {
        currentStickerOnTop = notification.object as? Sticker
        currentStickerOnTopCenter = currentStickerOnTop!.center
        artworkArea.bringSubview(toFront: currentStickerOnTop!)
    }
    
    func removeStickerFromStage()
    {
        currentStickerOnTop!.removeFromSuperview()
        currentStickerOnTop = nil
        
        numOfStickersOnStage -= 1
        
        if numOfStickersOnStage > 0
        {
            resetStickersTags()
        }
        else if drawView.brushstrokes.count == 0
        {
            saveArtworkButton.isEnabled = false
            clearArtworkButton.isEnabled = false
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Pan Gesture
    //-------------------------------------------------------------------------//
    
    func handlePan(_ pgr: UIPanGestureRecognizer)
    {
        if pgr.state == .ended
        {
            if !currentStickerOnTop!.isInsideStageBounds()
            {
                removeStickerFromStage()
            }
            else
            {
                currentStickerOnTopCenter = currentStickerOnTop!.center
            }
            
            return
        }
        
        let translationOnSuperview = pgr.translation(in: artworkArea)
        let posX = currentStickerOnTopCenter.x + translationOnSuperview.x
        let posY = currentStickerOnTopCenter.y + translationOnSuperview.y
        let currentCenterOnSuperview = CGPoint(x: posX, y: posY)
        currentStickerOnTop!.center = currentCenterOnSuperview
        currentStickerOnTop!.checkPosition()
    }

    //-------------------------------------------------------------------------//
    // MARK: IBActions
    //-------------------------------------------------------------------------//
    
    @IBAction func stickersButtonTapped(_ sender: UIButton)
    {
        showingContView = .Stickers
        performContainerViewAnimation(true)
        
        let name = NSNotification.Name(rawValue: "StickersVCExposed")
        center.post(name: name, object: nil)
    }

    @IBAction func brushSettingsButtonTapped(_ sender: UIButton)
    {
        showingContView = .BrushSettings
        performContainerViewAnimation(true)
    }
    
    @IBAction func stageColorsButtonTapped(_ sender: UIButton)
    {
        performStageColorsViewAnimation(true)
    }
    
    @IBAction func saveArtworkButtonTapped(_ sender: UIButton)
    {
        let status = PHPhotoLibrary.authorizationStatus()
        
        guard status == .authorized else
        {
            if let customDialog = CustomDialog.instanceFromNib(.AlbumPermission)
            {
                customDialog.delegate = self
                customDialog.setCustomDialogFrame()
                let back = customDialog.getCustomDialogBackground()
                view.addSubview(back)
                view.addSubview(customDialog)
            }
            
            return
        }
        
        if let customDialog = CustomDialog.instanceFromNib(.PromptArtworkSaving)
        {
            customDialog.delegate = self
            customDialog.setCustomDialogFrame()
            let back = customDialog.getCustomDialogBackground()
            view.addSubview(back)
            view.addSubview(customDialog)
        }
    }
    
    @IBAction func clearArtworkButtonTapped(_ sender: UIButton)
    {
        if let customDialog = CustomDialog.instanceFromNib(.StartOver)
        {
            customDialog.delegate = self
            customDialog.setCustomDialogFrame()
            let back = customDialog.getCustomDialogBackground()
            view.addSubview(back)
            view.addSubview(customDialog)
        }
    }
    
    @IBAction func pickAnotherPaperButtonTapped(_ sender: UIButton)
    {
        if let customDialog = CustomDialog.instanceFromNib(.PickAnotherPaper)
        {
            customDialog.delegate = self
            customDialog.setCustomDialogFrame()
            let back = customDialog.getCustomDialogBackground()
            view.addSubview(back)
            view.addSubview(customDialog)
        }
    }

    @IBAction func undoButtonTapped(_ sender: UIButton)
    {
        drawView.performUndo()
    }

    @IBAction func redoButtonTapped(_ sender: UIButton)
    {
        drawView.performRedo()
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton)
    {
        performStageColorsViewAnimation(false)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Animations
    //-------------------------------------------------------------------------//
    
    private func performContainerViewAnimation(_ show: Bool)
    {
        var dumping: CGFloat = 0.5
        
        if !show
        {
            dumping = 0.8
        }
        
        UIView.animate(withDuration: 0.5, delay: 0.0,
        usingSpringWithDamping: dumping, initialSpringVelocity: 10.0,
        options: .curveEaseOut,
        animations:
        {
            if show
            {
                if self.showingContView == .BrushSettings
                {
                    self.brushSettingsContViewTopConstraint.constant = 0
                }
                else
                {
                    self.stickersContViewTopConstraint.constant = 0
                }
            }
            else
            {
                let h = UIScreen.main.bounds.height
                
                if self.showingContView == .BrushSettings
                {
                    self.brushSettingsContViewTopConstraint.constant = h * -1
                }
                else
                {
                    self.stickersContViewTopConstraint.constant = h * -1
                }
            }
            
            self.view.layoutIfNeeded()
        },
        completion:
        {
            finished -> Void in
        })
    }
    
    private func performStageColorsViewAnimation(_ show: Bool)
    {
        UIView.animate(withDuration: 0.5, delay: 0.0,
        usingSpringWithDamping: 0.2, initialSpringVelocity: 10.0,
        options: .curveEaseOut,
        animations:
        {
            if show
            {
                self.stageColorsViewBottomConstraint.constant = 0
                self.toolbarBottomConstraint.constant = -100.0
                self.toolbar.alpha = 0
            }
            else
            {
                self.stageColorsViewBottomConstraint.constant = -100.0
                self.toolbarBottomConstraint.constant = 0
                self.toolbar.alpha = 1
            }
            
            self.view.layoutIfNeeded()
        },
        completion:
        {
            (finished) -> Void in
        })
    }
    
    //-------------------------------------------------------------------------//
    // MARK: CustomDialogDelegate
    //-------------------------------------------------------------------------//
    
    func giveAlbumPermission()
    {
        let status = PHPhotoLibrary.authorizationStatus()
        
        if (status == .denied) || (status == .restricted)
        {
            Auxiliar.albumAccessErrorDialog(status: status, vc: self)
        }
        else // status == .notDetermined
        {
            PHPhotoLibrary.requestAuthorization({
                
                status in
                
                DispatchQueue.main.async
                {
                    if status == .authorized
                    {
                        self.saveArtwork()
                    }
                }
            })
        }
    }
    
    func saveArtwork()
    {
        Auxiliar.showLoadingHUDWithText("Saving Artwork...", view: view)
        
        if let imageRep = artworkArea.getImageRepresentation()
        {
            if let croppedImg = imageRep.cropSelfTo(cropArea: drawView.frame)
            {
                UIImageWriteToSavedPhotosAlbum(croppedImg, self,
                #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
                
                return
            }
        }
        
        Auxiliar.hideLoadingHUDInView(view: view)
        
        if let customDialog = CustomDialog.instanceFromNib(.ArtworkNotSaved)
        {
            customDialog.delegate = self
            customDialog.setCustomDialogFrame()
            let back = customDialog.getCustomDialogBackground()
            view.addSubview(back)
            view.addSubview(customDialog)
        }
    }
    
    func startOver()
    {
        clearDrawing()
    }
    
    func pickAnotherPaper()
    {
        clearDrawing()
        drawView.drawingVC = nil
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Artwork savement callback
    //-------------------------------------------------------------------------//
    
    func image(_ image: UIImage, didFinishSavingWithError error: Error?,
               contextInfo: UnsafeRawPointer)
    {
        Auxiliar.hideLoadingHUDInView(view: view)
        
        if error == nil
        {
            if let customDialog = CustomDialog.instanceFromNib(.ArtworkSaved)
            {
                customDialog.delegate = self
                customDialog.setCustomDialogFrame()
                let back = customDialog.getCustomDialogBackground()
                view.addSubview(back)
                view.addSubview(customDialog)
            }
            
            clearDrawing()
        }
        else
        {
            if let customDialog = CustomDialog.instanceFromNib(.ArtworkNotSaved)
            {
                customDialog.delegate = self
                customDialog.setCustomDialogFrame()
                let back = customDialog.getCustomDialogBackground()
                view.addSubview(back)
                view.addSubview(customDialog)
            }
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Clear drawing
    //-------------------------------------------------------------------------//
    
    private func clearDrawing()
    {
        drawView.clearUndo()
        drawView.clearRedo()
        clearStickers(true)
        
        saveArtworkButton.isEnabled = false
        clearArtworkButton.isEnabled = false
        
        let dic: [String : Any] = ["ColorCode" : drawView.brushstrokeColorCode,
                                   "Thikness" : drawView.brushstrokeThickness]
        
        let name = NSNotification.Name(rawValue: "PerformedUndoOrRedo")
        center.post(name: name, object: dic)
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Stickers on stage handling
    //-------------------------------------------------------------------------//
    
    private func clearStickers(_ removeAll: Bool)
    {
        guard numOfStickersOnStage > 0 else
        {
            return
        }
        
        let max = removeAll ? numOfStickersOnStage : 10
        var stickersOnStage = [Sticker]()
        
        for subview in artworkArea.subviews
        {
            if subview.tag >= 500
            {
                stickersOnStage.append(subview as! Sticker)
                
                if stickersOnStage.count == max
                {
                    break
                }
            }
        }
        
        var counter = stickersOnStage.count - 1
        
        while counter >= 0
        {
            stickersOnStage[counter].removeFromSuperview()
            numOfStickersOnStage -= 1
            counter -= 1
        }
        
        if numOfStickersOnStage > 0
        {
            resetStickersTags()
        }
    }
    
    private func resetStickersTags()
    {
        var counter = 0
        
        for subview in artworkArea.subviews
        {
            if subview.tag >= 500
            {
                subview.tag = 500 + counter
                counter += 1
                
                if counter == numOfStickersOnStage
                {
                    currentStickerOnTop = subview as? Sticker
                    currentStickerOnTopCenter = currentStickerOnTop!.center
                }
            }
        }
    }
    
    //-------------------------------------------------------------------------//
    // MARK: UICollectionViewDataSource
    //-------------------------------------------------------------------------//
    
    func collectionView(_ view: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return Constants.hexColors.count
    }
    
    func collectionView(_ cv: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let hexColor = Constants.hexColors[indexPath.item]
        let color = UIColor(hex: hexColor)
        
        let cell = cv.dequeueReusableCell(withReuseIdentifier: "PaintCell", for: indexPath) as! PaintCell
            cell.circle.applyMask(maskImageName: "CircleMask")
            cell.paint.applyMask(maskImageName: "PaintSplashMask")
            cell.circle.backgroundColor = color
            cell.paint.backgroundColor = color
        
        if hexColor == stageColorCode
        {
            cell.circle.isHidden = true
            cell.paint.isHidden = false
        }
        else
        {
            cell.circle.isHidden = false
            cell.paint.isHidden = true
        }
        
        return cell
    }
    
    //-------------------------------------------------------------------------//
    // MARK: UICollectionViewDelegate
    //-------------------------------------------------------------------------//
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let hexColor = Constants.hexColors[indexPath.item]
        stageColorCode = hexColor
        drawView.backgroundColor = UIColor(hex: hexColor)
        collectionView.reloadData()
        
        let name = NSNotification.Name(rawValue: "StageColorChanged")
        center.post(name: name, object: stageColorCode)
    }

    //-------------------------------------------------------------------------//
    // MARK: Memory Warning
    //-------------------------------------------------------------------------//
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
        //Remove os stickers que estão mais embaixo (10 no máximo):
        clearStickers(false)
    }
}
