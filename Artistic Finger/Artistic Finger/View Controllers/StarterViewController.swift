//
//  StarterViewController.swift
//  Artistic Finger
//
//  Created by Nissi Vieira Miranda on 6/7/17.
//  Copyright © 2017 App Magic. All rights reserved.
//

import UIKit

class StarterViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var stageTypeButton: UIButton!
    
    @IBOutlet weak var pickPaperColor: UIImageView!
    @IBOutlet weak var pickPaper: UIImageView!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var stepOneViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var stepOneViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var stepTwoViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var stepTwoViewWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var stageTypeButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var stageTypeButtonHeightConstraint: NSLayoutConstraint!
    
    private var counter = 1
    private var pickedStageType: StageType = .Landscape
    private var pickedStageColorCode = "ffffff"
    
    private var landDimensions: [CGFloat] = [502.0, 316.0]
    private var squaredDimensions: [CGFloat] = [316.0, 316.0]
    private var panoDimensions: [CGFloat] = [502.0, 158.0]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        let w = UIScreen.main.bounds.width
        
        stepOneViewWidthConstraint.constant = w
        stepTwoViewWidthConstraint.constant = w
        stepTwoViewLeftConstraint.constant = w
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        
        let w = UIScreen.main.bounds.width
        
        stepOneViewLeftConstraint.constant = 0
        stepTwoViewLeftConstraint.constant = w
        
        pickPaper.isHidden = false
        pickPaperColor.isHidden = true
        backButton.isHidden = true
        doneButton.isHidden = true
        nextButton.isHidden = false
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    //----------------------------------------------------------------------------------//
    // MARK: IBActions
    //----------------------------------------------------------------------------------//
    
    @IBAction func stageTypeButtonTapped(_ sender: UIButton)
    {
        if counter <= 2
        {
            counter += 1
        }
        else
        {
            counter = 1
        }
        
        switch counter
        {
            case 1:
                pickedStageType = .Landscape
                performStageAnimation()
            
            case 2:
                pickedStageType = .Squared
                performStageAnimation()
            
            case 3:
                pickedStageType = .Panoramic
                performStageAnimation()
            
            default:
                print("\n Unknown stage type \n")
        }
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton)
    {
        performTransitionAnimation(false)
    }
    
    @IBAction func doneButtonTapped(_ sender: UIButton)
    {
        performSegue(withIdentifier: "StarterToDrawing", sender: nil)
    }
    
    @IBAction func nextButtonTapped(_ sender: UIButton)
    {
        performTransitionAnimation(true)
    }
    
    //----------------------------------------------------------------------------------//
    // MARK: Animations
    //----------------------------------------------------------------------------------//
    
    private func performStageAnimation()
    {
        UIView.animate(withDuration: 0.5, delay: 0.0,
        usingSpringWithDamping: 0.2, initialSpringVelocity: 10.0,
        options: .curveEaseOut,
        animations:
        {
            switch self.pickedStageType
            {
                case .Landscape:
                    self.stageTypeButtonWidthConstraint.constant = self.landDimensions[0]
                    self.stageTypeButtonHeightConstraint.constant = self.landDimensions[1]
                    
                case .Squared:
                    self.stageTypeButtonWidthConstraint.constant = self.squaredDimensions[0]
                    self.stageTypeButtonHeightConstraint.constant = self.squaredDimensions[1]
                    
                case .Panoramic:
                    self.stageTypeButtonWidthConstraint.constant = self.panoDimensions[0]
                    self.stageTypeButtonHeightConstraint.constant = self.panoDimensions[1]
                    
                default:
                    print("\n Unknown stage type \n")
            }
                
            self.view.layoutIfNeeded()
        },
        completion:
        {
            (finished) -> Void in
        })
    }
    
    private func performTransitionAnimation(_ movingForward: Bool)
    {
        UIView.animate(withDuration: 1.0, delay: 0.0,
        usingSpringWithDamping: 0.2, initialSpringVelocity: 10.0,
        options: .curveEaseOut,
        animations:
        {
            let w = UIScreen.main.bounds.width
            
            if movingForward
            {
                self.stepOneViewLeftConstraint.constant = w * -1
                self.stepTwoViewLeftConstraint.constant = 0
            }
            else
            {
                self.stepOneViewLeftConstraint.constant = 0
                self.stepTwoViewLeftConstraint.constant = w
            }
            
            self.view.layoutIfNeeded()
        },
        completion:
        {
            (finished) -> Void in
            
            if movingForward
            {
                self.pickPaper.isHidden = true
                self.pickPaperColor.isHidden = false
                self.backButton.isHidden = false
                self.doneButton.isHidden = false
                self.nextButton.isHidden = true
            }
            else
            {
                self.pickPaper.isHidden = false
                self.pickPaperColor.isHidden = true
                self.backButton.isHidden = true
                self.doneButton.isHidden = true
                self.nextButton.isHidden = false
            }
        })
    }
    
    //----------------------------------------------------------------------------------//
    // MARK: UICollectionViewDataSource
    //----------------------------------------------------------------------------------//
    
    func collectionView(_ view: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return Constants.hexColors.count
    }
    
    func collectionView(_ cv: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let hexColor = Constants.hexColors[indexPath.item]
        let color = UIColor(hex: hexColor)
        
        let cell = cv.dequeueReusableCell(withReuseIdentifier: "PaintCell", for: indexPath) as! PaintCell
            cell.circle.applyMask(maskImageName: "CircleMask")
            cell.paint.applyMask(maskImageName: "PaintSplashMask")
            cell.circle.backgroundColor = color
            cell.paint.backgroundColor = color
        
        if hexColor == pickedStageColorCode
        {
            cell.circle.isHidden = true
            cell.paint.isHidden = false
        }
        else
        {
            cell.circle.isHidden = false
            cell.paint.isHidden = true
        }
        
        return cell
    }
    
    //-------------------------------------------------------------------------//
    // MARK: UICollectionViewDelegate
    //-------------------------------------------------------------------------//
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let hexColor = Constants.hexColors[indexPath.item]
        let color = UIColor(hex: hexColor)
        
        pickedStageColorCode = hexColor
        stageTypeButton.backgroundColor = color
        
        collectionView.reloadData()
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Prepare for segue
    //-------------------------------------------------------------------------//
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let vc = segue.destination as! DrawingViewController
            vc.stageType = pickedStageType
            vc.stageColorCode = pickedStageColorCode
    }
    
    //-------------------------------------------------------------------------//
    // MARK: Memory Warning
    //-------------------------------------------------------------------------//
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
