//
//  StickerCell.swift
//  Artistic Finger
//
//  Created by Nissi Vieira Miranda on 5/10/17.
//  Copyright © 2017 App Magic. All rights reserved.
//

import UIKit

class StickerCell: UICollectionViewCell
{
    @IBOutlet weak var stickerImageView: UIImageView!
}
