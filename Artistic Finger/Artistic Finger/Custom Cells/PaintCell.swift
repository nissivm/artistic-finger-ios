//
//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  PaintCell.swift
//  Artistic Finger
//
//  Created by Nissi Vieira Miranda on 05/12/14.
//  Copyright (c) 2014 App Magic. All rights reserved.
//

import UIKit

class PaintCell: UICollectionViewCell
{
    @IBOutlet weak var circle: UIView!
    @IBOutlet weak var paint: UIView!
}
